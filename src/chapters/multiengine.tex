\chapter{Multiengine Back-end Implementation with LiSA}
\label{chapter:multiengine}

In contrast with the other back-end implementations available for LiSA, which
have to define the functions that are contained by \texttt{struct
switch_operations}, the \textit{multiengine} attaches a new layer to the
implementation. Its role is to aggregate multiple pieces of back-end
and configure them in a transparent manner. Due to this new layer, the
architecture of LiSA has changed, but it maintained the generic aspect.


\section{Multiengine Architecture}
\label{sec:multiengine-arch}

A new layer, the \textit{aggregator} layer, has been added between the CLI and
the \textit{switch} API previously created. It will become the new mediator between
the two entities. The initial API will be preserved to ensure an uniform
access to the pieces of back-end. The \textit{multiengine}  was designed taking
into consideration the possibility to also control switches implemented with
LiSA, located on remote hosts.

The \textit{mulitengine} offers to the user a transparent way for managing
multiple switches implemented based on LiSA . There were some elements that had to be considered:
\begin{itemize}
  \item how will the \textit{multiengine} be aware of what back-end
    implementations will have to manage
  \item what happens if multiple switches have the same name for the interfaces, how will be
  identified
  \item the management of virtual interface, on which switch will be an interface
    added
\end{itemize}

As a solution for identifying the switches that have to be unified, a
configuration file has to be filled. There was the option to write a regular
file and to impose formatting rules, in order to parse it or to use a known file
format whose structure allows a name - value association. The latter proposal
was chosen, because the users are familiar with the format 
and there were already implemented libraries that one can use to generate
as well as to parse
the configuration file. This solution comes to help the ones who will be using the
application, due to the possibility to automate the process of generating the
file, without having to write it from scratch when something changes. More
information about the content of the file will be given in
\labelindexref{Section}{sec:multiengine-impl} where implementation
details are discussed.

\abbrev{JSON}{JavaScript Object Notation}

\begin{center}
\fig[scale=0.32]{src/img/LiSA-arch/multiengine-arch.pdf}{img:multiengine-arch}{Architecture
of multiengine back-end for LiSA}
\end{center}

Now that the new API has information about the entities to be managed, the
list can be passed to the CLI, to display indexes of the switches,  in order
to inform the user what
entities he can configure. The indexing solves the problem regarding multiple
interfaces with the same name on different switches. Attaching the number
associated to the switch to the name of the interface there will be no
confusion because there can not be two interfaces with the same name on the
same device.


But beside receiving commands from the CLI, the main purpose of the
\textit{multiengine} is the communication with the other back-end
implementations. If the switches are located on the same device as the
\textit{multiengine}, there is no need to use \texttt{sockets} or other means of
communication. The shared object obtained for the implementation is available
on the same device and can be accessed directly(as can be observed from
\labelindexref{Figure}{img:multiengine-arch}). For the remote host,
connection parameters to the host/device on which the back-end
implementation resides can be found in the configuration file.


For this back-end in particular, no configuration information is kept in the
shared memory. It only commands the other back-end implementations and they will
manage information in their own specific way, without any intervention from
the outside.

The \textit{aggregator} API will substitute the \textit{switch} API. Hence, through it,
all the back-end implementations will be managed. Even if there
exists only one back-end implementation, it will be registered to the
\textit{multiengine} and it will be the only instance controlled.

\section{Multiengine Implementation}
\label{sec:multiengine-impl}

One of the reasons for not preserving the initial API is that even if it was
generic, it was not flexible for handling multiple back-end implementations
at the same time. The \textit{switch} API functions, which are members of \texttt{struct
switch_operations}, receive as a parameter a pointer to the structure with the
same name. The structure received as a parameter contains pointers to the
functions implemented using the features that the back-end provides.
Maintaining this behaviour, the CLI would have been not only a command executer,
it would have become also a manager of the unified back-end implementations. It
would have to determine the destination switch for a command and this means
more than being a command processor. To maintain a clear
delimitation between the layers of the generic switch implemented using LiSA, 
the \textit{aggregator} layer was introduced.

\subsection{CLI Changes}
\label{subsec:multi-cli-changes}

The responsibilities of the \textit{switch API} are taken by the new layer,
which materialized into a set of functions that are homonym to the ones defined for the previous API.
The difference stands in their behaviour. The ones which are part from the new mediator between CLI 
and the back-end implementations, can be considered as a connection multiplexor: they redirect the 
received commands to the intended addressee. The differences between the two versions of API can
be observed from the CLI commands, but also from the definition of the functions which parse them.


\begin{center}
\fig[scale=0.4]{src/img/LiSA-arch/command-ex-init-vs-multi.pdf}{img:cli-diff}{Differences
between the CLI commands}
\end{center}

As shown in \labelindexref{Figure}{img:cli-diff}, the format of the CLI commands also had to be adapted to support the \textit{multiengine back-end}.
For the ones that can be directed only to a certain entity, the index associated to a certain 
switch implementation has to be mentioned. If it is omitted, it will be considered by default
the switch with index `0'. A default behaviour was chosen for virtual interface adding.
This type of interfaces are added only on the first back-end implementation
which uses \textit{bridge and 8021q} kernel modules.


\subsection{Multiengine Back-end Initialization}
\label{subsec:multi-config}

Beside the API, other specific element of the discussed implementation is the
capability to manage multiple back-end implementations. As mentioned in
\labelindexref{Section}{sec:multiengine-arch}, a configuration file is used,
to be more specific, a JSON type file. For parsing it was used
\textit{cJSON}\footnote{\url{http://cjson.sourceforge.net/}},
an API which provides the basic functions to extract he nodes and their
content, but also to write a file having this type. The elements are
extracted from the nodes according to their type. By extracting their
values this way, it was facilitated the handling of the elements 
sent to the output by the cJSON API functions.

The parsing of the file is made only once, at the moment when
the application is launched. There is a function that initializes the
structures of the \textit{multiengine} and  also triggers the parsing process. It has the
attribute \textit{constructor} and this forces the function to be executed
before the function \texttt{main()}.

The information associated with a certain back-end implementation is stored in
a structure, which has as members the fields that can be found in the
configuration file. An example of configuration file can be found in the
\hyperref[appendix:configuration-file]{appendix}. A linked list of all the
structures that correspond to different pieces of back-end it is kept. Each
implementation has certain interfaces which correspond to it. Because the
number of interfaces can vary, a linked list was chosen to memorize this
association. The content of the above mentioned structure is captioned in the following
listing.

\lstset{language=C,basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
caption=Multiengine structures,label=lst:multiengine-structures}
\begin{lstlisting}
struct switch_interface {
  char if_name[MAX_NAME_SIZE];
  struct list_head lh;
};

struct backend_entries {
  int sw_index;
  char port[MAX_NAME_SIZE];
  char ip[MAX_NAME_SIZE];
  char type[MAX_NAME_SIZE];
  char locality[MAX_NAME_SIZE];
  struct switch_operations *sw_ops;
  struct list_head if_names_lh;
  struct list_head lh;
};
\end{lstlisting}

As mentioned before, the switch index will play an important part because it will
be the only way to distinguish between two interfaces with the same name from two
different pieces of back-end. The \texttt{port} and \texttt{ip} members of the structure
\texttt{backend_entries} are initialized only when the back-end is on a remote
host. The \texttt{locality} field is used for specifying whether the shared
object associated with an implementation of back-end is available on the
same device as the \textit{aggregator} (the value of locality is set to `local') or
on a remote device (the value of locality is set to `remote').

For obtaining the value of \texttt{sw_ops} field the parsing of the
configuration file was not the only prerequisite. The
\texttt{switch_operations} structure is specific to the \textit{switch} API,
that was preserved for the back-end implementations. By exposing this API, the
multiengine can access the functions implemented for a specific back-end.

The definitions of the \textit{switch} API functions are concentrated in a shared object, that in the former
generic switch implementations using LiSA was linked to the CLI module. Because this
connection was superseded, a new manner of accessing them had to be found. For
the case when the field \texttt{locality} from the structure
\hyperref[lst:multiengine-structures]{\texttt{backend_entries}} has the value `local',
\texttt{dlopen}\footnote{\url{http://pubs.opengroup.org/onlinepubs/009695399/functions/dlopen.html}}
function is used. In this way the shared object is made available through a handle to
the calling program. 

After obtaining access to the definitions contained by
the library, the pointer to the API functions has to be obtained.
This is possible due to a side effect of opening the library, the initialization function of the
shared object is called, which has associated the attribute
\textit{constructor}.

Its responsibility is to extract the pointer to the
\texttt{switch_operations} structure that stores the
context of a certain back-end implementation. The context contains details
specific to the back-end, but all contexts have in common the pointer to the
API functions. Beside being saved in the context structure, it is declared as
an extern global variable, named \texttt{sw_ops}. Its attributes extern and
global make possible to be obtained by calling \texttt{dlsym()} function at the moment when the
\textit{multiengine} back-end is initialized. The earlier mentioned function
looks-up variables or functions from a shared object and returns the address
where symbol is loaded. The pointer obtained by calling \texttt{dlsym()}
functions is stored in the structure associated to the back-end
implementation.

Going further with analysing the members of the \texttt{backend_entries}
structure, the
\texttt{if_names_lh} element can be observed. It is the head of the list which contains the names
of the interfaces associated with the back-end implementations. The names are
extracted from the configuration file. The structure used for storing the
information about an interface is named \texttt{switch_interface} and it only
contains the name of the interface and the link to the next member of the
list.

The last member of the structure \texttt{backend_entries}, \texttt{lh}, is used for ensuring the linkage
between the pieces of information about each back-end implementation described
in the configuration file. The type of the element is \texttt{struc
list_head}, the same as the one used for kernel lists. This is not a coincidence,
the kernel lists API was also implemented in user space for LiSA, facilitating list operations.


Saving the information it is done by parsing only once the configuration file.
The data is saved as the file is parsed node by node, including the pointer
to the implementation of the \textit{switch API}.

After this initialization step, all the information is available to the
\textit{aggregator} API functions and can be used for implementing the desired
functionalities.


\subsection{Aggregator API Implementation}
\label{subsec:aggregator-api}


The \textit{aggregator} API implementation is not back-end dependant.
Each API function has the purpose to redirect the received command to the
corresponding back-end. A reference to the list containing the back-end
details is held. When a function is called, according to the received
parameters, a look-up is made to determine the switch to which the command is
addressed. The headers of the API functions can be found in
\labelindexref{Appendix}{appendix:aggregator-api}.

The functions can be grouped into categories, according to the targeted
entities:
\begin{itemize}
  \item Functions for configuring interfaces
  \item Functions for VLAN management
  \item Functions for configuring the protocols
  \item Functions for general switch configurations
\end{itemize}

  A function that configures interfaces has to direct the command to a certain
  switch. The addressee is determined using the index of the switch received
  as a parameter. The index can be set to a default value, this indicating
  that the targeted LiSA switch is the first one from the list kept by the
  \textit{aggregator} layer. Otherwise, it is looked-up the switch that has
  the index received as parameter. To obtain the desired configuration, it is
  called the corresponding \textit{switch} API function from \texttt{struct
  swicth_operations}.


\lstset{language=C,basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
caption=Implementation of the function for interface adding from \textit{aggregator}
API,label=lst:if-add}
\begin{lstlisting}
  int if_add(int sw_index, char *if_name, int mode)
  {
    int if_index, sock_fd;
    struct backend_entry *sw_entry;

    /* extract switch entry from the linked list */
    sw_entry = get_switch_entry(index);

    if_index = if_get_index(if_name, sock_fd);

    return sw_entry->sw_ops->if_add(sw_entry->sw_ops, if_index, mode);
  }
\end{lstlisting}

  In \labelindexref{Listing}{lst:if-add}, the interface adding functionality is
  described. The function \texttt{get_switch_entry} is responsible for
  searching the switch associated with a certain index. Because the interface is
  identified by name and the switch API functions receive the index of the
  interface, the function \texttt{if_get_index} is used to make this
  translation. After obtaining all the necessay elements, the corresponding \textit{switch}
  API function is called.

  There is a particular type of interfaces that are added only on LiSA
  switches implemented using the kernel modules \textit{8021q.ko} and
  \textit{bridge.ko}: the virtual interfaces. This approach was chosen because
  these modules use the interfaces for inter-vlan routing.


  The commands for VLAN management are not dependant of a certain switch and
  can be considered broadcast because are sent to all the switches that
  are part of the \textit{multiengine}. For adding a VLAN, the list of
  switches is iterated through and for each is called the function
  \texttt{vlan_add}. If an error ocurrs while adding a VLAN on a certain
  switch, the ones that were successfully added are deleted from the
  corresponding switches. A snippet of code illustrating the mentioned
  functionality can be found in \labelindexref{Listing}{lst:vlan-add}:


\lstset{language=C,basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
caption=Implementation of the function for vlan adding from \textit{aggregator}
API,label=lst:vlan-add}
\begin{lstlisting}
int vlan_add(int vlan)
{
  int status, sw_idx;
  struct backend_entry *entry;

  status = 0;

  list_for_each_entry(entry, &head_sw_ops, lh) {
    status = entry->sw_ops->vlan_add(entry->sw_ops, vlan);
    if (status != 0) {
      sw_idx = entry->sw_idx;
      goto del_vlan;
    }
  }

del_vlan:
  list_for_each_entry(entry, &head_sw_ops, lh) {
    if (entry->sw_index >= sw_idx)
      break;

   entry->sw_ops->vlan_del(entry->sw_ops, vlan);
  }

  return status;
}

\end{lstlisting}

  The instruction \texttt{list_for_each_entry} from
  \labelindexref{Listing}{lst:vlan-add} is a macro definition that is part of
  the API for handling the linked lists, which is similar to the one used in
  the Linux kernel.

  The functions used for showing the configuration of the switch, will in fact
  gather information from all the switches that are part of the
  \textit{multiengine}. The pieces of information are unified and displayed to
  the user. For implementing this functionality, the list of switches is
  iterated through and a specific function is called, according to the
  information that is necessary.

  To configure the networking protocols (CDP and RSTP), there is no need to identify a
  certain switch. All the components of the  \textit{multiengine} are
  set up with the same parameters. The implementation is similar to the
  one used for managing  VLANs.

  The \textit{multiengine} back-end brought significant changes to the
  architecture of LiSA by introducing the \textit{aggregator} layer. It can be
  used when a single back-end implementation is available, but also with
  multiple back-end implementations. Due to the multiple back-end support,
  changes had to be done in order to adapt
  the CLI implementation to the new format of the commands.
  The implementation encapsulated by the API functions
  does not need to be changed when other pieces of back-end are
  added. This module will be a start point for developing further the project.
