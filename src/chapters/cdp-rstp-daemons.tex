\chapter{CDP and RSTP Protocols in LiSA Implementation}
\label{chapter:daemons}

The protocols are used in networking to enable a consistent communication
between endpoints. A protocol is a set of rules known by all the entities which
participate in the process of exchanging messages. Its implementation
should not affect the behaviour, the output should be the same.

``The nice thing about standards is that you have so many to choose
from.''\cite{tan-pc}. From those many choices that Andrew Tannenbaum
mentioned, for LiSA two of them were chosen to be implemented: CDP and RSTP.
For better understanding the implementation details of these two protocols, a
brief presentation of their functioning mechanism will be made.

\section{CDP Overview}
\label{sec:cdp-protocol-overview}

CDP is the acronym for Cisco Discovery Protocol. It is a propriety protocol, developed by Cisco as
a Data Link Layer protocol\footnote{Data Link Layer is the second layer 
  in the seven layer OSI model. In TCP/IP model corresponds to the 
\textit{link layer}.}. Being a layer two protocol, ``two connected systems which run
protocols at different network layers can learn about each
other'' \cite{cisco-cdp}.
It runs between network entities directly connected
(switches, routers, stations, remote access devices, IP telephones etc.). The
purpose of the protocol is to obtain the protocol address and the operating
system version of the neighbouring devices. CDP is not routable and can be
used only between devices which are directly connected. It is enabled by
default on all Cisco equipments.

CDP uses specific messages, named advertisements or announcements, which are sent periodically
to a multicast\footnote{An multicast address is used for sending the same
message at once to a group of devices from one source.} destination address
(01:00:0C:CC:CC:CC). The receivers of these packets are any Cisco devices
directly connected to the sender or any device that runs CDP on their
interface connected to the sender. By default the spacing between the
advertisements is 60s. The information received in an announcement is kept in
a table, in order to learn about the neighbours and determine when the
interfaces they expose go up or down. Each entry has a holdtime attached, signifying its lifetime. By
default the holdtime is 180s. If no announcements are received in this period
from the neighbour, the entry is discarded. On the other hand, when an
advertisement is received its holdtime is updated. The information contained
by an announcement can vary according to the version of the running operating
system or the type of the device.


\section{RSTP Overview}
\label{sec:rstp-protocol-overview}

RSTP is a protocol introduced by IEEE as the standard
\textit{802.1w}. Its purpose is to ensure a loop-free topology in bridged
networks. The protocol
is based upon STP\abbrev{STP}{Spanning Tree Protocol} which eventually became
obsolete since IEEE standard 802.1D-2004 embedded RSTP. The evolved version of
STP, RSTP, ensures backward compatibility with the initial version. The
difference between the two of them is that the latter converges faster after
topology changes.

\abbrev{BPDU}{Bridge Protocol Data Unit}

The functionality of this protocol is based on discovering the topology of the
network using special frames named BPDU(Bridge Protocol Data Unit). These
are highly used for initializing the switches and later for verifying if there
were any updates in the network. Also, there are multiple states which can be
assigned to a switch port:
\begin{itemize}
  \item \textit{Discarding} - the port is not used for sending data over the network
    and does not learn MAC addresses.
  \item \textit{Learning} - at this state MAC addresses learning is enabled, but frame
    forwarding is disabled. MAC tables are created by analysing the
    MAC addresses contained by the incoming frames.
  \item \textit{Forwarding} - the port is fully operational, it receives and forwards
    data frames and BPDUs, also, it updates the MAC table with MAC addresses.
  \end{itemize}

Each port can be assigned roles by RSTP:
\begin{itemize}
  \item \textit{Root port} - a forwarding port that is the closest from a root
    bridge to a non-root one.
  \item \textit{Designated port} - ``A port is designated if it can send the
    best BPDU on the segment to which it is connected.''\cite{cisco-rstp}
  \item \textit{Alternate and Backup ports} - are used as backup ports for root
    and designated ports.
  \end{itemize}

  To build the spanning tree there are three necessary steps: the root of
  the tree must be chosen (\textit{root bridge}), followed by the choosing of
  the root ports and of the active ports.\cite{rl}


\section{CDP and RSTP Implementation for the Initial Architecture of LiSA}
\label{sec:cdp-rstp-lisa}

To continuously improve the capabilities of LiSA a new feature was considered:
usage of network protocols to exchange data with the other devices. Until now
two protocols were implemented: CDP and RSTP.

RSTP was implemented as a diploma project by Andrei Faur in 2009 and CDP was
implemented by the ones who started the project. Beside the details concerning
the implementation of the protocols functionality (which will not be discussed in this chapter), they also had to
be linked with the implementation of LiSA.

In order to benefit from the usage of the implemented protocols, the user has
to be given access to enable, configure and retrieve information about their
running parameters. The CLI is the one which intermediates the communication
between the two entities.

The aspects to focus on are: the communication mechanism used between the CLI and
the protocols and how the protocols retrieve data from the network. How this
is implemented can be observed in the following figure.

\begin{center}
\fig[scale=0.45]{src/img/LiSA-arch/daemons-communication.pdf}{img:daemons-communication}{Integration
of CDP and RSTP with the initial LiSA implementation}
\end{center}

The protocols are standalone programs that can be started from the Makefile
which builds the application or by running them one by one. It was chosen for the
protocols to be daemons\footnote{A \textit{daemon} is a process that runs in
background, does not have a terminal attached and a user cannot directly
interact with it.} because they do not need an attached terminal to enable the
access of a user. The two daemons run multiple threads. Each thread is specialized on receiving
commands from CLI, sending specific network packets or receiving packets from
the network.


As mentioned before, multiple LiSA configuration sessions can be opened at the same
time on the same station. This type of behaviour has as a side effect the need
of sharing certain information between all the active instances. Hence, an IPC
method had to be used. As one can observe from
\labelindexref{Figure}{img:daemons-communication}, shared memory was the choice which seemed the most
suitable.


\lstset{language=C,basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
caption=Shared memory information,label=lst:switch_mem}
\begin{lstlisting}
struct shared_memory {
  /* Enable secrets (crypted) */
  struct {
    char secret[SW_SECRET_LEN + 1];
  } enable[SW_MAX_ENABLE+1];
  /* Line vty passwords (clear text) */
  struct {
    char passwd[SW_PASS_LEN + 1];
  } vty[SW_MAX_VTY + 1];
  /* CDP configuration */
  struct cdp_configuration cdp;
  /* RSTP configuration */
  struct rstp_configuration rstp;
  /* List of interface tags */
  struct mm_list_head if_tags;
  /* List of vlan descriptions */
  struct mm_list_head vlan_descs;
  /* List of interface descriptions */
  struct mm_list_head if_descs;

  /* List of VLAN specific data */
  struct mm_list_head  vlan_data;
  /* List of interface  specific data */
  struct mm_list_head  if_data;
  ........
};
\end{lstlisting}

Beside information about the interfaces, the VLANs or about the passwords, the
information about the configuration of a protocol must be the same on all
instances. An example of configuration information would be the state of
the protocol: enabled or disabled, or other elements specific to the protocol (for
example for CDP: version, holdtime). Because any entity can modify these
information, exclusive access has to be provided. For LiSA was also implemented a
locking mechanism to use shared memory.

But the information from shared memory is not enough for CDP, because one
is not interested only in viewing the configuration information. Other
interesting pieces of information would be the neighbours or the status of an interface.
To ensure the communication with
the daemon of the protocol, \textit{message queues}\footnote{\textit{Message queues} are
used for interprocess communication. They offer an asynchronous protocol of
communication between two endpoints. The messages are placed onto the
queue and are stored until the recipient retrieves the message.} are used. The
information is requested by the CLI and the CDP thread dedicated to hande CLI
requests will render it and send an appropriate response.

Until now the connection between the CLI and the protocol daemons has been
explained. The other important aspect, which created an issue when the
architecture was modified to make LiSA a generic switch, is the communication
with the kernel space, used for retrieving packets from the network.

The word protocol suggests that a certain behaviour is expected and a certain format of
the packets. This principle also applies to CDP and RSTP implementations for LiSA.
In order to receive only the packets intended for the protocol the
\texttt{AF_SWITCH}
sockets are used.


\lstset{language=C,basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
caption=Setting up a socket for filtering CDP
packets,label=lst:setup_sw_socket}
\begin{lstlisting}
static int setup_switch_socket(int fd, char *ifname) {
  struct sockaddr_sw addr;

  memset(&addr, 0, sizeof(addr));
  addr.ssw_family = AF_SWITCH;
  strncpy(addr.ssw_if_name, ifname, sizeof(addr.ssw_if_name)-1);
  addr.ssw_proto = ETH_P_CDP;

  if (bind(fd, (struct sockaddr *)&addr,
  sizeof(addr))) {
    perror("bind");
    close(fd);
    return -1;
  }
  fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK);
  return 0;
}
\end{lstlisting}

When the socket is attached to a port, it is specified the type of packets
to be received using that connection. In this way the packet filtering
responsibility rests with the kernel, more precise with the \textit{lisa.ko}
module. The filtering is done the same as above for the RSTP. Moreover,
RSTP uses \textit{ioctl()} calls over \texttt{AF_SWITCH} to obtain information from
kernel space. For this kernel dependence it had to be found a solution in order
to obtain a generic switch implementation.


\abbrev{IPC}{Inter Process Communication}

\section{Integrating CDP and RSTP Daemons with LiSA Generic Software Switch}
\label{sec:daemons-generic-sw}

Due to the fact that LiSA became a back-end independent switch,
\texttt{AF_SWICTH}
sockets can be used only when the back-end is \textit{LiSA}. As an alternative
for the other implementations \textit{raw sockets} and \textit{library pcap} were used.

\subsection{Raw Sockets in Linux}
\label{subsec:raw-sockets}

Raw sockets are a way of bypassing the sending and receiving of Internet
Protocols packets by not specifying any information about transport layer. On
standard sockets, the information transmitted is encapsulated according to the
specified protocol (e.g. TCP, UDP). On raw sockets no TCP/IP processing is done,
the packets are transmitted in a raw form. 

The receiver of the packet is
responsible for analysing the headers and the content, a job that usually the
kernel handles. Raw sockets are supported by POSIX sockets which are available
on all Linux distributions. This ensures flexibility for new back-end
implementations.

\abbrev{POSIX}{Portable Operating System Interface}

Handling packets through raw sockets implies a great responsibility, because
when a packet is sent to the kernel, it does not put any headers. This has to
be done explicitly by the sender. A packet with the wrong header is a packet
that will, most likely, be dropped or will reach to the wrong destination.

The structure of the packets to be sent is known for the implemented
protocols and the information to be encapsulated into the packet is also
available at sending time. Having all the necessary elements, raw sockets can
be used for sending packets instead of \texttt{AF_SWITCH} sockets.


\subsection{Packet Filtering Using \textit{libpcap}}
\label{subsec:libpcap}

Because communication in a protocol means sending packets, but also receiving
them from the network, a method for handling incoming packets was searched.
\textit{Pcap library} was considered a proper solution.

\textit{Libpcap} is a library which provides a high level interface for
capturing system packets. For better understanding how this library can be
used for network sniffing\footnote{A \textit{network sniffer} is a computer
program that can intercept and/or log traffic passing through a network.}, the flow of an application will be presented:
\begin{enumerate}
  \item First, one should establish the name of the interface on which the
    application will sniff on. The provided name should be a string or if the
    name of the interface is not known, the library can provide it.
  \item Initialize \textit{pcap} by furnishing it the device to sniff on. The device
    was obtained at the previous step. Multiple devices can be sniffed on at the
    same time. This is possible because for each session a different handle is
    obtained.
  \item For sniffing only a certain type of traffic a
    filter\footnote{For more details about \textit{pcap} filters the following
    link can be consulted \url{http://docs.nimsoft.com/prodhelp/en_US/Probes/Catalog/net_traffic/1.3/index.htm?toc.htm?1925170.html}} should be built.
    A filter is a string built using terms that the library can recognize. The
    string must be converted in a format that \textit{pcap} can read, because it is
    compiled. There is no need of an extra application to compile the
    filter, because \textit{pcap} provides a function to handle this. Then,
    the pcap session is informed to use the new created filter.
  \item The effective capture is done: a loop can be used to receive a
    preset number of packets or only one packet can be grabbed.
  \item Close the session.
\end{enumerate}

The usage of this library offers an user space alternative for filtering
network packets. Hence, it can be integrated with the generic implementation
of LiSA. Still, there is a drawback: the flexibility of the filters. The set
of elements from which a filter can be composed might not cover all the
possibilities. This would lead to receiving all the packets, with no filter
attached to \textit{pcap}, and dissect them to analyse if their content
matches the expected information.


\subsection{Network Protocols in the Generic Software Switch LiSA}
\label{subsec:current-lisa}

Network protocols implemented for LiSA do not have a separate implementation
for each back-end version. The way in which the communication with kernel
space is managed has been adapted according to the back-end that uses it.

\begin{center}
\fig[scale=0.38]{src/img/LiSA-arch/daemons-communication-current-LiSA.pdf}{img:daemons-communication-current}{CDP
and RSTP communication with the generic implementation of LiSA}
\end{center}

From \labelindexref{Figure}{img:daemons-communication-current} one can observe
that shared memory is preserved without any modification, because the
information stored there is not affected by the back-end implementations. If
multiple pieces of back-end run at the same time, the memory access is protected
using locks, so there will not be race conditions\footnote{A race condition
arises when multiple threads or processes attempt to operate over the same shared
resource. This behaviour can lead to unexpected results, corrupting the data.}.

CDP daemon keeps the message queues to exchange information with the CLI. The
modifications brought were at the level of communication between user space CDP
implementation and the kernel space. Instead of opening \texttt{AF_SWITCH}
sockets, raw sockets are used from \texttt{AF_PACKET} family. Raw packets can
be sent and received to the Data Link Layer (OSI Layer 2). These sockets are
used only for sending packets.

A filter is needed in order to receive only
packets which are destined to the protocoli. \textit{Pcap library} offers
support for filtering networks packets. Due to this feature, it was used to
receive and filter CDP packets. The filter was written, using the terminology specific to
\textit{pcap} library. The conditions that a packet has to meet to be a CDP
packet are: the destination Ethernet address must be multicast, the Ethernet
source must be
different from the station which sends the packet and the protocol ID must
correspond to SNAP. The ID is needed because any protocol that supports
SNAP\footnote{\url{http://standards.ieee.org/getieee802/download/802-2001.pdf}} within its frame can run CDP.

\abbrev{SNAP}{Subnetwork Access Protocol}

\lstset{language=C,basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
caption=Pcap filter for CDP,label=lst:cdp_filter}
\begin{lstlisting}
#define CDP_FILTER    "ether multicast and ether[20:2] = 0x2000 and ether src not %02hx:%02hx:%02hx:%02hx:%02hx:%02hx"
\end{lstlisting}


The RSTP protocol is a more complex protocol, because of the states in which the
interfaces should be put and the Linux kernel does not offer support for
these states. At the moment the protocol can be used with \textit{LiSA
back-end}. It can also be implemented for \textit{bridge and
8021q back-end} because the kernel modules with the same name offer support
for this protocol. Modifying the already existing implementation for
the back-end based on the two above mentioned Linux modules would bring LiSA
back  to the issue which led to the generic switch implementations in the
first place: writing a new implementation for an already existing functionality.

\textit{Libvirt} back-end is meant to connect LiSA with the virtual
machines, to access their interfaces. No switching mechanism is needed to
assure this connection. For this reason, the two protocols are not
necessary for this particular back-end.


The procedure of adapting the protocol implementations in order to be used
with the generic switch architecture of LiSA was different from the one of
translating the initial LiSA architecture. Decisions about implementation
details had to be made, not about how to redesign the implementations of CDP
and RSTP. As a solution, \textit{raw sockets} and \textit{pcap library} were found, but these can
not be considered to be universal. Each back-end may need to do further
changes in order to obtain the desired behaviour or other pieces of back-end
will not require the two protocols.
