\chapter{LiSA Overview}
\label{chapter:lisa-overview}

As mentioned in \labelindexref{Section}{sec:lisa-gss}
from \labelindexref{Chapter}{chapter:intro}, the initial
architecture of LiSA suffered some changes along the time. In order to observe the
modifications and to acknowledge their necessity, a review of each architecture will be made in the next sections.

\section{Initial LiSA Architecture}
\label{sec:initial-arch}
The purpose of LiSA was to offer a switching solution for systems which run Linux
as an operating system. A generic presentation of LiSA architecture can be
observed in the following figure (\labelindexref{Figure}{img:user-kernel}):

\fig[scale=0.4]{src/img/LiSA-arch/user-kernel.pdf}{img:user-kernel}{Generic
LiSA architecture}

The architecture covers both kernel space and user
space. The functionality of each component is well delimited:
the mechanism for packet switching is implemented in kernel space and user space is
responsible for configuring, monitoring and controlling the switching process.

When the user space was designed, it was taken into consideration that one might
open multiple configuration sessions at a time. For this type of behaviour
race conditions are most likely to appear, so a locking mechanism had to be
used in order to maintain the consistency of configurations.

All the decisions regarding the infrastructure of LiSA were based on the
principle that the functionality of a component
is not replicated on other parts of the implementation.

More architectural details can be observed in
\labelindexref{Figure}{img:lisa-init-arch} where the elements which compose each
entity are specified and also the links that exist between them.

\fig[scale=0.75]{src/img/LiSA-arch/Initial-arch.pdf}{img:lisa-init-arch}{Initial
LiSA Architecture}

The kernel space contains the core of LiSA, the kernel module, which is
responsible for the packet switching mechanism. To obtain
this functionality the module can be divided into subcomponents:
 \begin{itemize}
   \item \textit{Switching Engine}: it is the foundation of the module due to
     the fact that it receives the packets, makes switching decisions and
     applies algorithms used for efficiently communicating with the ports of the
   switch.
 \item \textit{Forwarding Database (FDB)}:  every switch should contain such a
   database or table for creating the CAM table \footnote{CAM table is table
   held by a network switch to map MAC addresses to ports.}.
  \item \textit{VLAN Database (VDB)}: contains all the necessary information
    for VLAN routing.
  \item \textit{VLAN interfaces (VIFs)}: necessary for implementing VLAN
    routing using the functionalities that Linux already offers.
 \end{itemize}

The link between the user space application and the LiSA kernel module is made
through
\textit{ioctl()} function calls. The kernel module implements the
functionality for the functions requested through \textit{ioctl()}.

To benefit from the facilities that the switch can offer, the user has to
interact with the user space component. One of the most important pieces from
user space is the command interpretor. The functionalities are packed into a
shared
library\footnote{\url{http://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html}}
which exposes the functions necessary to manage a switch. The elements which
were used to obtain the shared library are the following:
\begin{itemize}
  \item \textit{The shell}: is used as an input for the application.
  The user configures the switch through the commands written in
  the shell.
  The Readline\footnote{\url{http://cnswww.cns.cwru.edu/php/chet/readline/rltop.html}}
  library is used because offers word completion features for commands and it also  maintains a history of the commands.
\item \textit{Parser}: the commands introduced are identified and validated in
  order to be executed by the \textit{Command Runner}.
\item \textit{Command Runner}: is the entity which transforms the input
  received from the user into an actual switch action. For each CLI command
  there is a \textit{ioctl()} function call.  As mentioned before this is the
  way in which the information from user space is passed to kernel space. The
  kernel module component which handles \textit{ioctl} calls will be the one to make the
  changes in the switch configuration.
  \end{itemize}

  As mentioned in the general description of LiSA, the design should allow
  multiple management sessions to run simultaneously. Hence, configuration
  information should be shared between all the instances. The information
  which is specific to the kernel space component (switching information) is
  shared by default between all instances, the problem appears in user space.
  To solve this inconvenience, an IPC solution was used: shared memory. All the
  information that should be shared: enabled users, passwords, VLAN
  description and interface description can be found in a shared memory
  segment which is protected using a lock, to ensure exclusive access to data.

  Beside the \textit{Command Line Interface}, the daemons which implement the protocols CDP
  and RSTP, are also part from user space. In order to benefit from the
  functionalities exposed of these protocols, they have to be launched before
  starting a switch management session. As seen on
  \labelindexref{Figure}{img:lisa-init-arch}, the CLI will communicate using
  message queues with the daemons. Because the daemons need
  to communicate with the network layer for sending and receiving packets,
  sockets were necessary. Moreover, packet filtering was necessary and from this reason
  \texttt{AF_SWITCH} sockets were designed for LiSA. This implied modifying kernel
  sources, not only adding a new module. More details about the daemons and
  the sockets used for communication will be exposed in
  \labelindexref{Chapter}{chapter:daemons}.

  Having the kernel module, \textit{lisa.ko}, and the shared object,
  \textit{liblisa.so} is not enough because the user cannot invoke directly
  their functions. An executable, named \textit{swcli} was created, which makes
  some initial configurations before passing the access to the command
  interpretor. It can be started from any Linux standard shell and the user is
  given the highest privilege level. To run the executable, the user must have
  root privileges.

  Although LiSA provided the behaviour required for a software switch, when
  those who initiated the project considered integrating it in a Linux
  distribution, they received a negative answer. The motivation was that there
  already exist two Linux modules(\textit{bridge} and \textit{8021q}) which combined can offer the same
  functionality. This led to the idea of making LiSA adaptable for multiple
  back-end implementations, this meaning that instead of the kernel module
  \textit{lisa.ko} other implementations can be used. This new solution meant
  creating a \hyperref[sec:gen-sw]{Generic Software Switch based on LiSA}.

   \abbrev{FDB}{Forwarding Database}
   \abbrev{VDB}{Vlan Database}
   \abbrev{CAM}{Content Addressable Memory}
   \abbrev{IPC}{Inter Process Communication}


\section{LiSA as a Generic Software Switch}
\label{sec:gen-sw}

In \labelindexref{Figure}{img:lisa-init-arch} one could observe that the
communication between the CLI and the kernel module is done without any
mediator, using \textit{ioctl} calls over \texttt{AF_SWITCH} sockets. Using these
sockets which were designed for LiSA, the CLI was unable to support other
back-end implementations. 

There also existed an other problem regarding the implementation of the
command line interface. The  CLI makes one think about a commands executor,
the instructions are received as in input, their syntax is analysed  and some
``magic'' happens behind, this means that the CLI should not
be aware of how things are implemented. But in the initial LiSA implementation
it was also responsbile for implementing the functionality of the command.
There was not set a bound between command processing at a syntactic level and the
effective implementation of the functionality.


Due to the features implemented by the switching engine from kernel space,
discarding the module \textit{lisa.ko} was not considered as an option.
The CLI also brought LiSA a big advantage because it made the interaction with the
switch user friendly. In order to keep the old functionality, but also
to be able to use other switching engines beside \textit{lisa.ko}, an intermediate
layer was introduced, named Middleware.

The details of the new architecture can be seen in the following figure:

\fig[scale=0.43]{src/img/LiSA-arch/Current-arch-LiSA.pdf}{img:lisa-current-arch}{Current
LiSA architecture}

Putting side by side the former LiSA architecture and the current one, it can
be observed that the CLI does not communicate directly with the kernel. The
middleware will be a mediator between these two entities. Beside this aspect,
an important improvement is that ioctl calls are not the default way to
communicate with the kernel. Each middleware implementation which is made for
a certain back-end, uses its specific means to transmit the commands in kernel
space.

One of the drawbacks of the initial design was the usage of
\texttt{AF_SWITCH} sockets for ioctl calls, but also for assuring the functionality of
the daemons used for implementing the network protocols(CDP, RSTP). The solution found
was the usage of \textit{raw sockets} combined with the functionality of
\textit{libpcap}\footnote{\url{http://www.tcpdump.org/pcap.html}}
library. Raw sockets can be used on any Linux distribution this meaning they
would be available for any banck-end implementation. The protocols need to
communicate with the kernel to send and receive notification packets.

Because the shared memory was not dependent on the nature of the
implementation,
no changes had to be made. The same data has to be shared between
multiple switch sessions, no matter the nature of the implementation. The
difference is that shared memory modifications will not be made from CLI, but
from the implementation of the middleware.

Theoretically, for all the inconveniences a solution was found, but an effective
implementation had to be made. From this reason LiSA API was created to
fulfill the middleware functionality.


From an application which had two basic components (the command interpretor
and the kernel module from kernel space), LiSA became a
multilayer switch containing elements which accomplish a well established
purpose:
\begin{itemize}
  \item \textit{Command Line Interface}: used only for receiving commands from
    the switch. It calls middleware functions for obtaining the desired
    functionality, without having any knowledge about the back-end.
  \item \textit{Middleware}: contains the API called by the CLI to obtain the
    desired switch configuration. Also, its functions have to be implemented by each
    back-end to offer support for LiSA
  \item \textit{Middleware implementation}: each back-end version will
    implement the API, putting up the software switch behaviour using its own
    features.
  \end{itemize}

After introducing the middleware layer, LiSA became a generic software switch
which is able to sustain different back-end implementations by virtue of the
new layer.

The \textit{switch} API, also named LiSA API, was shaped as a structure which contains 
pointers to functions that should be implemented by each middleware version.
The members of the structure correspond to a command line instruction and there also exists
a function which initializes the elements specific for each back-end. The
definition of the structure can be found in the \labelindexref{Appendix}{lst:switch-api}.
Each middleware implementation will contain the earlier mentioned structure
and define the content of its functions.


The \textit{Command Line Interface} will change the behaviour of its functions. The
direct link with the kernel through \textit{ioctl} \footnote{\url{http://man7.org/linux/man-pages/man2/ioctl.2.html}} call will be eliminated, being
replaced by generic calls. The difference can be observed in the following
figure which contains a snippet from the CLI function which handles the adding
of Ethernet interfaces:


\fig[scale=0.42]{src/img/LiSA-arch/Sw-ops-example.pdf}{img:sw-ops-example}{Example
  of Switch API call from CLI}

In the former LiSA implementation, the CLI implemented the functionality by calling an ioctl function. It was not flexible for using other
pieces of back-end, being strictly connected to the \textit{lisa.ko} module which
contained the handlers for the \textit{ioctl} functions.

From \labelindexref{Figure}{img:sw-ops-example} one can observe that the CLI
does not call a back-end specific function in LiSA implementation
as a generic switch. An API function is called and back-end details are
not made available to the CLI. Only the middleware implementation is aware of the
back-end specific details. To be able to use the generic functions, the CLI
keeps a reference to a \textit{switch_operations} structure, whose content
points to the functions implemented using details specific to the back-end.


Using this approach, when a new back-end is implemented, it will not have to
modify the CLI because the function call will be the same. This makes the CLI
independent from the implementation layer.

With the generic switch implementation ready, the possibility of developing
various pieces of back-end was explored. In the next section the approaches
that were considered until now will be presented.


\section{LiSA Back-end Implementations}
\label{sec:back-impl}

The back-end implementation which seemed the most suitable for testing the new
architecture was the one based on \textit{lisa.ko} module, also named
\textbf{LiSA back-end}. Each function
from the structure \textit{switch_operations} was implemented and pointers to
these implementations were set to ensure that the CLI uses
the proper function definitions. All the API functions were implemented using \textit{ioctl()} 
calls which were handled by the \textit{ioctl handlers} implemented in the kernel
module. The new design did not affect the former LiSA kernel module.
Just a few changes were brought to the initial implementation of the module,
but without a significant meaning. This back-end version still uses
\texttt{AF_SWITCH}
sockets for the \textit{ioctl} calls and for implementing the protocols (CDP and
RSTP).


The reason why LiSA was modified to be a generic switch in the first place, became a source of
inspiration for the second back-end implementation. The problem was that Linux already has two
modules \textit{bridge} and \textit{8021q} which combined can offer the same
functionality as \textit{lisa.ko} module. This back-end implementation named
\textbf{bridge  + 8021q} is based on the modules with the same names offered by
Linux. Like all the back-end versions, this also has to implement the
functions contained by the \textit{switch_operations} structure. The
difference is that it uses \textit{ioctl()} calls with sockets and requests specific
to the two kernel modules provided by Linux. Other particularity of this implementation is the
usage of \textit{netlink
sockets}\footnote{\url{http://qos.ittc.ku.edu/netlink/html/}} for some
functionalities. Also, the \texttt{AF_SWITCH} sockets are not available for this
implementation, hence \textit{raw sockets} were used because they are available on any
Linux distribution.


Beside the earlier mentioned back-end possibilities, other considered option
was integrating LiSA with OpenWrt\footnote{OpenWrt is a Linux distribution for embedded
devices.}. This back-end implementation was considered so that LiSA can offer
support for hardware devices which have memory constraints and can run a
small size Linux distribution, but also for SoC (Systems on a
Chip\footnote{\url{http://whatis.techtarget.com/definition/system-on-a-chip-SoC}}).
Using LiSA, one can control the Ethernet switch integrated in these types of
equipments. A drawback of this back-end is that it does not offer support for
the protocols.


From integrating LiSA on an embedded device the ideas went to a higher level:
integrating LiSA with a virtualization API, to be able to control multiple
virtual machines. For this purpose
\textit{libvirt}\footnote{\url{http://wiki.libvirt.org/page/Main_Page}} was chosen. The back-end
implementation using \textit{libvirt} makes LiSA to be an option for
controlling networks composed from virtual machines.


Having all these back-end possibilities that can be independently controlled,
one might think why not control all these back-end implementations at the
same time, in a transparent way for each of them?

To handling this aspect, a new back-end was
implemented, the so called \textbf{multiengine}. It can be
considered a controller for the switches which it aggregates. A new level was
introduced in order to obtain this functionality, a new API which contains a series
of functions. The old \textit{switch API} was kept for all the other pieces of
back-end. As one can observe on
\labelindexref{Figure}{img:multiengine-overview}, the configurations can be
changed using the  CLI attached to the multiengine, and the switches that compose the
multiengine can not be accessed through the CLI.

\fig[scale=0.5]{src/img/LiSA-arch/multiengine-overview.pdf}{img:multiengine-overview}{Multiengine
General Structure}

More details about the multiengine architecture and implementation can be
found in the chapter dedicated to this subject.

Through the years LiSA implementation improved. From a switch with a back-end
dependent implementation it became a generic switch that can be adapted to use
multiple back-end implementations (LiSA back-end, Bridge + 8021q back-end,
intergration with OpenWrt, usage of LiSA with libvirt for controlling virtual
mahcines and the
multiengine back-end). Each of these implementations inherited from the
original implementation the CLI, but also the protocols CDP and RSTP which had
to be adapted in order to function on all the back-end implementations. How
the protocols were initially implemented and what needed to be changed is an
aspect discussed in the next chapter.
