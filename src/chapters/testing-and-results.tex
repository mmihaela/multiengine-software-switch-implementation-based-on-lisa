\chapter{Testing and Results}
\label{chapter:test}

To be able to test the functionality of the \textit{multiengine} back-end, the
component switches must provide the desired behaviour. Beside being able to
run as a standalone switch, each LiSA switch, no matter the nature of the
back-end, should be able to expose the desired functionality even when it is linked
to another switch implemented using LiSA.

Before verifying the compatibility between two different types of back-end,
unit testing was performed for each back-end. The unit tests were written
along with the implementation of the functions for the LiSA back-end. It was
convenient manner of testing the implemented functionalities without
integrating it yet with the CLI. The unit tests can be considered a
simplified Command Line Interface, the input commands being an one to one
association with the switch API functions. For accessing the tests,
\textit{swctl} executable should be run (it is available with the project).
In \labelindexref{Appendix}{appendix:swctl-test} a snippet of code containing
the implementation of the test which verifies interface adding can be found.

After verifying that the implementation of the \textit{switch} API functions
it works, a compatibility test it was made. The purpose of the test was to verify
that multiple LiSA switches with different back-end implementations can work
together.

The back-end implementations used for this test were \textit{bridge + 8021q}
back-end (implemented using two Linux kernel modules) and \textit{LiSA}
back-end (based on \textit{lisa.ko} kernel module). The first of them was
installed on a workstation on which Debian OS was running, with the kernel
version 3.2. The latter was installed on a CentOS, with the kernel version
2.6. To be able to run the back-end implemented using \textit{lisa.ko} module,
additional packages had to be installed.\footnote{The packages and the
install steps can be found at the following
address:\url{https://github.com/lisa-project/lisa-user/wiki/Install-and-run-LISA-using-kernel-rpm-packages}}

The stations used for hosting the switches were equipped with two network
cards: one of them having the capacity of 100Mbit/s and the other of 1Gbit/s.

The topology of the network used for testing contains three switches: two of
them running a LiSA switch that uses \textit{bridge + 8021q} back-end and the
other runs a LiSA switch with \textit{LiSA} back-end. There are also four
stations linked as can be seen in the next figure:

\begin{center}
\fig[scale=0.43]{src/img/LiSA-arch/test-topo.pdf}{img:test-topology}{Topology
  of the network used for testing}
\end{center}


To test all the features that LiSA can offer, VLANs where configured on hosts
and on the interfaces associated with the switches. The inter-switch links
were configured in \texttt{trunk} mode. The VLANs allowed were VLAN 3 and 4. The
interface of the host connected to a certain switch was added into a VLAN an
set into \texttt{access} mode. The hosts were also given IP addresses from the same network to enable the
communication between them, using only a Data Link Layer device.

After running the \textit{swcli} executable on the stations on which LiSA
switches were installed, the configurations were made: the interfaces were
added to the switch, the VLANs were also added and the next step was to put
the interfaces in trunk mode as well as to establish what VLANs are allowed on
the trunk links.

With the switches configured, \texttt{ping} utility was used to test the
connectivity between the hosts. This enabled the process of learning the MAC
addresses on each switch. In \labelindexref{Table}{table:mac-addr-table} the
output of the command \texttt{show mac-address-table} is displayed.

\begin{table}[htb]
  \caption{Mac address table on switch number two (SW2)}
  \label{table:mac-addr-table}
\begin{tabular}{l*{9}{c}r}
  Destination Address              & Address Type & Vlan & Destination Port \\
  \hline
  00e0.208c.01ed  &   Dynamic    &      4 & eth2 \\
  00e0.2082.44d9  &   Dynamic    &      4 & eth2 \\
  00e0.2082.44d9  &   Dynamic    &      3 & eth2 \\
  7071.bc18.1c1c  &   Dynamic    &      4 & eth2 \\
  7071.bc18.1bff  &   Dynamic    &      3 & eth2 \\
  7071.bc18.1bff  &   Dynamic    &      4 & eth2 \\
  7071.bc08.2588  &   Dynamic    &      4 & eth4 \\
  7071.bc08.257e  &   Dynamic    &      3 & eth3 \\
\end{tabular}
\end{table}


The switch learnt the destination address and the port that should be used for sending a
packet to a certain address. As one can observe from the
\hyperref[table:mac-addr-table]{table}, one MAC address is listed twice
because the trunk port can be used for sending packets in VLAN 3, but also in
VLAN 4.


Moving a step further,
\texttt{iperf3}\footnote{\url{https://code.google.com/p/iperf/}} utility was used to determine the
bandwidth performance. For testing two hosts were necessary, hence \texttt{HOST
1} and \texttt{HOST 3} were chosen, because it was desired that the packets pass
through two switches that run two different pieces of back-end. In order
to transmit a packet from \texttt{HOST 1} to \texttt{HOST 3}, SW1 and SW2 are
the ones that intermediate this connection. One host acted as a server (HOST
1) and the other as a client (HOST 3). The server received the connection and
the result of the analysis was outputted on the client side.

\begin{center}
  \fig[scale=0.5]{src/img/LiSA-arch/iperf3.pdf}{img:iperf3}{\textit{iperf3} usage on hosts}
\end{center}

In \labelindexref{Figure}{img:iperf3}, one could observe the commands that
were ran on each
station. On \texttt{HOST 3}, the \texttt{`-c'} parameter indicates that the host is a
client and it must be followed by the IP of the server. It was preserved the  default time to
transmit: 10s. The \texttt{`-J'} parameter requests the output in the JSON format.

The result was that 120586240 Bytes were sent in 10.1821 seconds and the
number of bits transmitted per second 94.7436Mbit/s. The following snippet is
part of the output in JSON format. Also it can be observed the CPU usage on
the localhost, but also on the remote host.

\lstset{language=C,basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
caption={Bandwith performance analysis using
\textit{iperf3}},label=lst:iperf3-performance}
\begin{lstlisting}
"streams":  [{
    "sent": {
      "socket": 4,
      "start":  0,
      "end":  10.1821,
      "seconds":  10.1821,
      "bytes":120586240,
      "bits_per_second": 9.47436e+07,
      "retransmits": 0
    },
    "received":
    {
      "socket": 4,
      "start":  0,
      "end": 10.1821,
      "seconds": 10.1821,
      "bytes": 120324096,
      "bits_per_second": 9.45377e+07
    }
  }],
"cpu_utilization_percent":
{
  "host" : 7.69756,
  "remote" : 3.7996
}
\end{lstlisting}

There was also another testing topology that involved two systems with LiSA
switches using \textit{lisa.ko} module and to hosts. Between the switches
there was a trunk connection. The transfer speed obtained by using
\textit{iperf3} was similar to the one obtained with the topology described
using \labelindexref{Figure}{img:test-topology}: 95,0919Mbit/s.


These tests have proved that multiple pieces of LiSA back-end can be used
together, independent of the nature of the back-end: all the switches can use
the same back-end implementation or different ones. Due to
the compatibility demonstrated using these tests, LiSA can be considered as
an option when it comes to choosing a switching device for small networks. Moreover, it comes as a confirmation that the \textit{multiengine} back-end has
applicability and it is adaptable, being able to integrate new back-end
implementations that are compatible with the ones which exist.
